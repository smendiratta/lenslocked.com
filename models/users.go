package models

import (
	"errors"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // just because
	"golang.org/x/crypto/bcrypt"

	"lenslocked.com/hash"
	"lenslocked.com/rand"
)

var (
	// ErrNotFound is returned when a resource cannot be
	// found int he database.
	ErrNotFound = errors.New("models: resource not found")

	ErrInvalidID = errors.New("models: ID provided was invalid")

	ErrInvalidPassword = errors.New("models: incorrect password provided")
)

const (
	userPwPepper  = "super-random-pepper-123"
	hmacSecretKey = "secret-hmac-key"
)

type User struct {
	gorm.Model
	Name         string
	Email        string `gorm:"not null,unique_index"`
	Password     string `gorm:"-"`
	PasswordHash string `gorm:"not null"`
	Remember     string `gorm:"-"`
	RememberHash string `gorm:"not null,unique_index"`
}

type UserDB interface {
	// Methods for querying for single users
	ByID(id uint) (*User, error)
	ByEmail(email string) (*User, error)
	ByRemember(token string) (*User, error)

	// Methods for altering users
	Create(user *User) error
	Update(user *User) error
	Delete(id uint) error

	// User to close a DB connection
	Close() error

	// Migration helpers
	// AutoMigrate() error
	DestructiveReset()
}

// UserService is a set of methods used to manipulate and
// work with the user model
type UserService interface {
	// Authenticate will verify the provided email address and
	// password are correct. If they are correct, the user
	// corresponding to that email will be returned. Otherwise
	// you will receive either:
	// ErrNotFound, ErrInvalidPassword, or another error if
	// something goes wrong.
	Authenticate(email, password string) (*User, error)
	UserDB
}

func NewUserService(connInfo string) (UserService, error) {
	ug, err := newUserGorm(connInfo)
	if err != nil {
		return nil, err
	}

	return &userService{
		UserDB: &userValidator{
			UserDB: ug,
		},
	}, nil
}

var _ UserService = &userService{}

type userService struct {
	UserDB
}

// Authenticate can be used to authenticate a user with the
// provided email address and password.
func (us *userService) Authenticate(email, password string) (*User, error) {
	foundUser, err := us.ByEmail(email)
	if err != nil {
		return nil, err
	}

	pwBytes := []byte(password + userPwPepper)
	err = bcrypt.CompareHashAndPassword([]byte(foundUser.PasswordHash), pwBytes)
	if err != nil {
		switch err {
		case bcrypt.ErrMismatchedHashAndPassword:
			return nil, ErrInvalidPassword
		default:
			return nil, err
		}
	}

	return foundUser, nil
}

var _ UserDB = &userValidator{}

type userValidator struct {
	UserDB
}

var _ UserDB = &userGorm{}

func newUserGorm(connInfo string) (*userGorm, error) {
	db, err := gorm.Open("postgres", connInfo)
	if err != nil {
		return nil, err
	}
	db.LogMode(true)
	hmac := hash.NewHMAC(hmacSecretKey)
	return &userGorm{
		db:   db,
		hmac: hmac,
	}, nil

}

type userGorm struct {
	db   *gorm.DB
	hmac hash.HMAC
}

// ByID will look up by the id provided.
// 1 - user, nil
// 2 - nil, ErrNotFound
// 3 - nil, otherError
func (ug *userGorm) ByID(id uint) (*User, error) {
	user := new(User)
	db := ug.db.Where("id = ?", id)
	err := first(db, user)
	return user, err
}

// ByEmail looks up a user with email address
// and return sthat user
func (ug *userGorm) ByEmail(email string) (*User, error) {
	user := new(User)
	db := ug.db.Where("email = ?", email)
	err := first(db, user)
	return user, err
}

// ByRemember looks up a user with the given remember token
// and returns that user. This method will handle hashing
// the token fo rug.
// Errors are the same as ByEmail
func (ug *userGorm) ByRemember(token string) (*User, error) {
	user := new(User)
	rememberHash := ug.hmac.Hash(token)
	db := ug.db.Where("remember_hash = ?", rememberHash)
	err := first(db, user)
	return user, err
}

// Create will create the provided user and backfill data
// like the ID, CreatedAt and UpdatedAt fields.
func (ug *userGorm) Create(user *User) error {
	pwBytes := []byte(user.Password + userPwPepper)
	hashBytes, err := bcrypt.GenerateFromPassword(pwBytes, bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.PasswordHash = string(hashBytes)
	user.Password = ""

	if user.Remember == "" {
		token, err := rand.RememberToken()
		if err != nil {
			return err
		}
		user.Remember = token
	}
	user.RememberHash = ug.hmac.Hash(user.Remember)

	return ug.db.Create(user).Error
}

// Update will update the provided user with all of the data
// in the provided user object.
func (ug *userGorm) Update(user *User) error {
	if user.Remember != "" {
		user.RememberHash = ug.hmac.Hash(user.Remember)
	}

	return ug.db.Save(user).Error
}

// Delete will delete the user with provided ID
func (ug *userGorm) Delete(id uint) error {
	if id == 0 {
		return ErrInvalidID
	}
	user := User{Model: gorm.Model{ID: id}}
	return ug.db.Delete(user).Error
}

// CLoses ug *userGorm database connection
func (ug *userGorm) Close() error {
	return ug.db.Close()
}

// DestructiveReset drops the user table and rebuilds it
func (ug *userGorm) DestructiveReset() {
	ug.db.DropTableIfExists(&User{})
	ug.db.AutoMigrate(&User{})
}

// first will query using the provided gorm.DB and it will
// get the first item retrurned and place it into dst. If
// nothing is found int he query, it will return ErrNotFound
func first(db *gorm.DB, dst interface{}) error {
	err := db.First(dst).Error
	if err == gorm.ErrRecordNotFound {
		return ErrNotFound
	}
	return err
}
